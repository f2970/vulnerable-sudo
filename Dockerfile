FROM debian:buster-slim
ADD ./src/ /data/
RUN sed -i 's/main/main contrib non-free/g' /etc/apt/sources.list
RUN apt-get update && apt-get -y upgrade && apt-get install -y dpkg-dev git gcc build-essential debhelper libpam0g-dev libldap2-dev libsasl2-dev libselinux1-dev autoconf bison flex libaudit-dev mandoc python3
# RUN cd && dpkg-source -x /data/sudo_1.8.27*.dsc && cd sudo-1.8.27 && dpkg-buildpackage -rfakeroot -b && dpkg -i ../sudo_1.8.27-1+deb10u2_amd64.deb
# RUN cd /data/CVE-2021-3156 && make all && chmod +x exploit.sh
RUN useradd -m -u 1000 -s /bin/bash debian
RUN echo debian:debian | chpasswd
# RUN usermod --password debian debian
RUN cd /data/sudo-deb && dpkg -i sudo_1.8.27-1+deb10u2_amd64.deb
USER debian